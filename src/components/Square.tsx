import React from "react";

export interface ISquareProps {
  value?: string;
  onClick: () => void;
}

export default class Square extends React.Component<ISquareProps> {
  render() {
    return (
      <button className="square" onClick={this.props.onClick}>
        {this.props.value}
      </button>
    );
  }
}
