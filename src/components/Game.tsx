import React from "react";
import Board from "./Board";

export interface IGameProps {
  columns: number;
  rows: number;
  winnerLength: number;
}
const defaultProps: IGameProps = {
  columns: 3,
  rows: 3,
  winnerLength: 3,
};

export interface IGameState {
  history: Array<{
    squares: Array<Array<string | undefined>>;
    player?: string;
  }>;
  historyIndex: number;
}

export default class Game extends React.Component<IGameProps, IGameState> {
  public static defaultProps = defaultProps;

  constructor(props: IGameProps) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array.from(
            {
              length: this.props.rows,
            },
            () => Array(this.props.columns)
          ),
          player: undefined,
        },
      ],
      historyIndex: 0,
    };
  }

  private handleSquareClick(r: number, c: number) {
    const history = this.state.history.slice(0, this.state.historyIndex + 1);
    const current = history[history.length - 1];
    const squares = current.squares.map((value) => value.slice());
    if (this.checkWinner(current.squares) || squares[r][c]) {
      return;
    }
    const player = getNextPlayer(current.player);
    squares[r][c] = player;
    this.setState({
      history: history.concat([
        {
          squares: squares,
          player: player,
        },
      ]),
      historyIndex: history.length,
    });
  }

  private checkWinner(squares: Array<Array<string | undefined>>) {
    return checkWinner(squares, this.props.winnerLength);
  }

  private setHistoryIndex(index: number) {
    this.setState({
      historyIndex: index,
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.historyIndex];
    const winner = this.checkWinner(current.squares);

    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + getNextPlayer(current.player);
    }

    const moves = history.map((value, index) => {
      const desc = index ? "Go to move #" + index : "Go to game start";
      return (
        <li key={index}>
          <button onClick={() => this.setHistoryIndex(index)}>{desc}</button>
        </li>
      );
    });

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onSquareClick={(r, c) => this.handleSquareClick(r, c)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

function getNextPlayer(player: string | undefined) {
  return player === "X" ? "O" : "X";
}

function checkWinner(
  squares: Array<Array<string | undefined>>,
  winnerLength: number
) {
  const rows = squares.length;

  for (let r = 0; r < rows; r++) {
    const columns = squares[r].length;

    for (let c = 0; c < columns; c++) {
      const square = squares[r][c];

      if (square) {
        // Square value is valid, all sequences start at 1
        let rowSequence = 1;
        let columnSequence = 1;
        let ltrDiagonalSequence = 1;
        let rtlDiagonalSequence = 1;

        for (let i = 1; i < winnerLength; i++) {
          const nextRow = r + i;
          const nextColumn = c + i;
          const previousColumn = c - i;

          // Checking rows
          if (nextRow < rows && square === squares[nextRow][c]) {
            rowSequence++;
          }

          // Checking columns
          if (nextColumn < columns && square === squares[r][nextColumn]) {
            columnSequence++;
          }

          // Checking left to right diagonal
          if (
            nextRow < rows &&
            nextColumn < columns &&
            square === squares[nextRow][nextColumn]
          ) {
            ltrDiagonalSequence++;
          }

          // Checking right to left diagonal
          if (
            nextRow < rows &&
            nextColumn >= 0 &&
            square === squares[nextRow][previousColumn]
          ) {
            rtlDiagonalSequence++;
          }
        }

        if (
          rowSequence === winnerLength ||
          columnSequence === winnerLength ||
          ltrDiagonalSequence === winnerLength ||
          rtlDiagonalSequence === winnerLength
        ) {
          return square;
        }
      }
    }
  }

  return undefined;
}
