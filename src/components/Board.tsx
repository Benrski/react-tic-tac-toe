import React from "react";
import Square from "./Square";

export interface IBoardProps {
  squares: Array<Array<string | undefined>>;
  onSquareClick: (r: number, c: number) => void;
}

export default class Board extends React.Component<IBoardProps> {
  render() {
    const rows = this.props.squares.length;
    const board = Array<JSX.Element>(rows);

    for (let r = 0; r < rows; r++) {
      const columns = this.props.squares[r].length;
      const squares = Array<JSX.Element>(columns);

      for (let c = 0; c < columns; c++) {
        squares[c] = (
          <Square
            key={r * columns + c}
            value={this.props.squares[r][c]}
            onClick={() => this.props.onSquareClick(r, c)}
          />
        );
      }

      board[r] = (
        <div key={r} className="board-row">
          {squares}
        </div>
      );
    }

    return <div>{board}</div>;
  }
}
